FROM alpine:latest
WORKDIR /app
ADD hello-boi.sh .
EXPOSE 6789
CMD nc -lk -p 6789 -e /app/hello-boi.sh

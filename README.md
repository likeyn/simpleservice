Simpleservice
=============

[![pipeline status](https://gitlab.com/likeyn/simpleservice/badges/master/pipeline.svg)](https://gitlab.com/likeyn/simpleservice/-/commits/master)

Just a simple, smol af HTTP service for testing purposes. Returns the value of `$HOSTNAME` in the container running it, wrapped in a JSON like so:

```
{"message":"Hello <hostname> boi"}
```

#!/bin/sh
CONTENT="{\"message\":\"Hello $HOSTNAME boi\"}"
echo "HTTP/1.1 200 OK"
echo "Cache-Control: no-cache, private"
echo "Content-Length: $(echo $CONTENT | wc -c)"
echo "Content-Type: application/json"
echo
echo $CONTENT
exit
